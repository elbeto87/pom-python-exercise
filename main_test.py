from trial_of_the_stones import TrialOfTheStone
from selenium import webdriver

#setup
browser = webdriver.Chrome('/home/egresia/Desktop/Otros/proyectos-py/pom-python/chromedriver')
trial_of_the_stone = TrialOfTheStone(driver=browser)
trial_of_the_stone.go()

#riddle of stone
trial_of_the_stone.riddle_of_stone_input.write('rock')
trial_of_the_stone.riddle_of_stone_button.click()
secret_answer = trial_of_the_stone.riddle_of_stone_answer.text()
assert secret_answer == 'bamboo'

#riddle_of_secrets
trial_of_the_stone.riddle_of_secrets_input.write(secret_answer)
trial_of_the_stone.riddle_of_secrets_button.click()
riddle_of_secrets_answer = trial_of_the_stone.riddle_of_secrets_answer.text()
assert riddle_of_secrets_answer == 'Success!'

#the_two_merchants
if int(trial_of_the_stone.the_two_merchants_jessica.text()) > int(trial_of_the_stone.the_two_merchants_bernard.text()):
    the_two_merchants_answer = 'Jessica'
else:
    the_two_merchants_answer = 'Bernard'
trial_of_the_stone.the_two_merchants_input.write(the_two_merchants_answer)
trial_of_the_stone.the_two_merchants_button.click()
assert trial_of_the_stone.the_two_merchants_answer.text() == "Success!"

#check_answers
trial_of_the_stone.check_answers_button.click()
assert trial_of_the_stone.check_answers_results.text() == "Trial Complete"

browser.quit()
