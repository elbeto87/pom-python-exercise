from base_element import BaseElement
from selenium.webdriver.common.by import By

class TrialOfTheStone:

    def __init__(self, driver):
        self.driver = driver
        self.url = 'https://techstepacademy.com/trial-of-the-stones'

    def go(self):
        self.driver.get(self.url)

    @property
    def riddle_of_stone_input(self):
        locator = (By.ID, 'r1Input')
        return BaseElement(driver=self.driver, by=locator[0], value=locator[1])

    @property
    def riddle_of_stone_button(self):
        locator = (By.ID, 'r1Btn')
        return BaseElement(driver= self.driver, by= locator[0], value= locator[1])

    @property
    def riddle_of_stone_answer(self):
        locator = (By.XPATH, "//div[@id='passwordBanner']")
        return BaseElement(driver=self.driver, by=locator[0], value=locator[1])

    @property
    def riddle_of_secrets_input(self):
        locator = (By.ID, 'r2Input')
        return BaseElement(driver= self.driver, by= locator[0], value=locator[1])

    @property
    def riddle_of_secrets_button(self):
        locator = (By.ID, 'r2Butn')
        return BaseElement(driver=self.driver, by=locator[0], value=locator[1])

    @property
    def riddle_of_secrets_answer(self):
        locator = (By.XPATH, "//div[@id='successBanner1']")
        return BaseElement(driver=self.driver, by=locator[0], value=locator[1])

    @property
    def the_two_merchants_jessica(self):
        locator = (By.XPATH, "//div/span/*[contains(text(),'Jessica')]/../../p")
        return BaseElement(driver=self.driver, by=locator[0], value=locator[1])

    @property
    def the_two_merchants_bernard(self):
        locator = (By.XPATH, "//div/span/*[contains(text(),'Bernard')]/../../p")
        return BaseElement(driver=self.driver, by=locator[0], value=locator[1])

    @property
    def the_two_merchants_input(self):
        locator = (By.ID, "r3Input")
        return BaseElement(driver=self.driver, by=locator[0], value=locator[1])

    @property
    def the_two_merchants_button(self):
        locator = (By.ID, "r3Butn")
        return BaseElement(driver=self.driver, by=locator[0], value=locator[1])

    @property
    def the_two_merchants_answer(self):
        locator = (By.ID, "successBanner2")
        return BaseElement(driver=self.driver, by=locator[0], value=locator[1])

    @property
    def check_answers_button(self):
        locator = (By.ID, "checkButn")
        return BaseElement(driver=self.driver, by=locator[0], value=locator[1])

    @property
    def check_answers_results(self):
        locator = (By.ID, "trialCompleteBanner")
        return BaseElement(driver=self.driver, by=locator[0], value=locator[1])



